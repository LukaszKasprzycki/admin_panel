Documentation created with React-Styleguidist.

https://github.com/styleguidist/react-styleguidist

Run npx styleguidist server to start a style guide dev server.

Run npx styleguidist build to build a static version.

Open index.html to see static documentation.
