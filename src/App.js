import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import AdminPanel from "./components/AdminPanel";
import LoginPanel from "./components/Logging/LoginPanel";
import Loading from "./components/Logging/elements/Loading";
import { API } from "./helpers";
/**
 * import MuiThemeProvider and createMuiTheme to create default app theme
 * @see {@link https://material-ui-next.com/customization/themes/}
 */
import { MuiThemeProvider, createMuiTheme } from "material-ui/styles";
import { connect } from "react-redux";
import { fetch } from "./actions";
/**
 * create MuiTheme instance
 */
const theme = createMuiTheme();
/**
 * add redux store as a props to App class
 * @param {Object} state-current redux state
 */
const mapStateToProps = state => ({
  textures: state.textures,
  photos: state.photos,
  frames: state.frames,
  script: state.script
});
/**
 * @file Application Container
 * @requires MuiThemeProvider,Router
 */
class App extends Component {
  /**
   * Creates default App state
   * @param {boolean} isLogged-show if user is logged
   * @param {undefined|Object} user-contain user data,undefined by default
   * @param {boolean} loading-show loading component
   */
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      user: undefined,
      loading: true
    };
    this.login = this.login.bind(this);
    this.logOut = this.logOut.bind(this);
  }
  /**
   * check if user is logged
   * @fires API.user
   * fetch data from server
   * @fires this.props.fetch
   *
   */
  componentDidMount() {
    API.user()
      .then(response => {
        if (response.data.id !== undefined) {
          this.setState({
            isLogged: true,
            user: response.data,
            loading: false
          });
        } else {
          this.setState({ isLogged: false, user: undefined, loading: false });
        }
      })
      .catch(error => {
        this.setState({ isLogged: false, user: undefined, loading: false });
      });
    this.props.fetch();
  }
  /**
   * Function log user into application
   * @public
   * @param {boolean} isLogged specify if user is logged
   * @param {Object} user user data
   */
  login(isLogged, user) {
    this.setState({ isLogged, user, loading: false });
  }
  /**
   * Function log user out of application
   */
  logOut() {
    this.setState({ isLogged: false, user: undefined, loading: false });
    API.logout();
  }
  /**
   * render App component with React Router
   * component matches current url with defined paths and renders specific Route
   * it uses protected routes
   * @see {@link:https://reacttraining.com/react-router/web/example/auth-workflow}
   * @requires Router,Route,Switch
   */
  render() {
    const { textures, frames, photos, script } = this.props;
    const { user, isLogged, loading } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route
              exact
              path="/login"
              render={({ history }) =>
                isLogged ? (
                  <Redirect to="/" />
                ) : loading ? (
                  <Loading />
                ) : (
                  <LoginPanel login={this.login} history={history} />
                )
              }
            />
            <Route
              path="/"
              render={({ history }) =>
                isLogged ? (
                  <AdminPanel
                    history={history}
                    theme={theme}
                    textures={textures}
                    frames={frames}
                    photos={photos}
                    script={script}
                    user={user}
                    logOut={this.logOut}
                  />
                ) : (
                  <Redirect to="/login" />
                )
              }
            />
          </Switch>
        </Router>
      </MuiThemeProvider>
    );
  }
}
/**
 * @exports HoC App connected to global store
 */
export default connect(mapStateToProps, { fetch })(App);
