import thunk from "redux-thunk";
import fetchMock from "fetch-mock";
import configureMockStore from "redux-mock-store";

import * as actions from "./actions.js";
describe("Synchronic actions", () => {
  test("should create an action to activate item in Redux", () => {
    const expectedResult = {
      type: `ACTIVATE`,
      payload: 10
    };
    expect(actions.activate(10)).toEqual(expectedResult);
  });
  test("should create an action to process item in Redux", () => {
    const expectedResult = {
      type: `PROCESS`,
      payload: 20
    };
    expect(actions.process(20)).toEqual(expectedResult);
  });

  test("should create an action to add an item to Redux", () => {
    const exampleItem = {
      id: 1,
      name: "example",
      createdAt: "2018-05-08 17:23:38",
      image: "uploads/textures/example.jpg",
      value: "uploads/textures/example.jpg"
    };
    const expectedResult = name => ({
      type: `ADD_${name}`,
      payload: exampleItem
    });
    expect(actions.add(exampleItem, "textures")).toEqual(
      expectedResult("TEXTURES")
    );
    expect(actions.add(exampleItem, "FRAMES")).toEqual(
      expectedResult("FRAMES")
    );
    expect(actions.add(exampleItem, "scRipt")).toEqual(
      expectedResult("SCRIPT")
    );
  });
  test("should create an action to remove an item from Redux", () => {
    const exampleItem = {
      id: 1,
      name: "example",
      createdAt: "2018-05-08 17:23:38",
      image: "uploads/textures/example.jpg",
      value: "uploads/textures/example.jpg"
    };
    const expectedResult = name => ({
      type: `REMOVE_${name}`,
      payload: exampleItem
    });
    expect(actions.remove(exampleItem, "textures")).toEqual(
      expectedResult("TEXTURES")
    );
    expect(actions.remove(exampleItem, "FRAMES")).toEqual(
      expectedResult("FRAMES")
    );
  });
  test("should create an action to store items in Redux", () => {
    const expectedResult = {
      type: `FETCH_EXAMPLE`,
      payload: []
    };
    expect(actions.storeToRedux([], "EXAMPLE")).toEqual(expectedResult);
  });
});

// describe("Async Action", () => {
//   afterEach(() => {
//     fetchMock.reset();
//     fetchMock.restore();
//   });
//   const middlewares = [thunk];
//   const mockStore = configureMockStore(middlewares);
//   const payload = [
//     {
//       id: 1,
//       name: "example",
//       createdAt: "2018-05-08 17:23:38",
//       image: "uploads/textures/example.jpg",
//       value: "uploads/textures/example.jpg"
//     },
//     {
//       id: 2,
//       name: "example2",
//       createdAt: "2018-05-08 18:23:38",
//       image: "uploads/textures/example2.jpg",
//       value: "uploads/textures/example2.jpg"
//     }
//   ];
//   test("creates FETCH_TEXTURES when fetching data has been done", () => {
//     fetchMock.get("/api/TEXTURES/", { body: { payload } });
//     const expectedActions = {
//       type: "FETCH_TEXTURES",
//       payload: payload
//     };
//     const store = mockStore({ state: [] });
//     console.log(actions.fetch(["TEXTURES"]));
//     return store.dispatch(actions.fetch(["TEXTURES"])).then(() => {
//       console.log(store.getActions());
//       expect(store.getActions()).toEqual(expectedActions);
//     });
//   });
// });
