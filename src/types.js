import {
  shape,
  number,
  string,
  oneOf,
  object,
  func,
  array,
  arrayOf,
  element,
  bool,
  oneOfType
} from "prop-types";
/**
 * USER
 */
export const userType = shape({
  user: shape({
    username: string,
    id: number
  })
});
export const basicValidators = {
  string,
  number,
  func,
  object,
  array,
  oneOf,
  element,
  bool,
  oneOfType
};
export const itemValidator = shape({
  id: number,
  name: string,
  preview: string,
  size: number,
  type: string
});

export const filtersValidator = arrayOf(
  shape({
    value: string,
    label: string
  })
);
export const dataValidator = arrayOf(
  shape({
    id: number,
    name: string,
    createdAt: string,
    image: string,
    value: string
  })
);

export const tableHeadValidator = arrayOf(
  shape({
    id: string,
    label: string,
    sortable: bool
  })
);
export const framesValidator = arrayOf(
  shape({
    id: number,
    name: string,
    createdAt: string,
    image: string,
    value: string
  })
);
export const photosValidator = arrayOf(
  shape({
    id: number,
    name: string,
    createdAt: string,
    image: string,
    value: string
  })
);
export const texturesValidator = arrayOf(
  shape({
    id: number,
    name: string,
    createdAt: string,
    image: string,
    value: string
  })
);
