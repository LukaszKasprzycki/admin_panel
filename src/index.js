/**
 * @file Entry point of admin_panel application
 * @author Łukasz Kasprzycki
 * @version 1.0.0
 */
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reducer from "./reducers/index";
import "typeface-roboto";
/**resets default browser behaviour
 * @see {@link http://meyerweb.com/eric/tools/css/reset/}
 */
import "./index.css";
/**
 * @constant composeEnhancers adds Redux DevTools extension
 * @see {@link https://github.com/zalmoxisus/redux-devtools-extension}
 */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/**
 * @constant store creates global store with thunk middleware
 * @see {@link https://github.com/zalmoxisus/redux-devtools-extension}
 */
const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
