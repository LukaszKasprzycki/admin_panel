/**
 * Created by lukasz on 01.03.18.
 */
import axios from "axios";
export function formatBytes(bytes) {
  if (bytes < 1024 * 1024) return (bytes / 1024).toFixed(2) + "KB";
  else return (bytes / (1024 * 1024)).toFixed(2) + "MB";
}
export function isEqualArray(first, second) {
  if (first.length !== second.length) return false;

  for (let i = 0; i < first.length; i++) {
    if (first[i] !== second[i]) return false;
  }

  return true;
}

export function getExtFromName(filename) {
  if (filename === undefined) return false;
  let arr = filename.split(".");

  const ext = arr.pop();
  const name = arr.join("_");

  return { ext, name };
}
export function formatDate(dateString) {
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  const date = new Date(dateString);
  const day = date.getDate();
  const month = months[date.getMonth()];
  const year = date.getFullYear();

  return `${day} ${month}, ${year}`;
}

export const schemas = {
  textures: {
    key: "file",
    successStatus: 201,
    store(data, name, func) {
      data.forEach(object => func(object, name));
    }
  },
  frames: {
    key: undefined,
    successStatus: 201,
    store(data, name, func) {
      data.forEach(object => func(object, name));
    }
  },
  script: {
    key: "script",
    successStatus: 200,
    store(data, name, func) {
      func(data, name);
    }
  }
};

const API_ROOT = "/api";
const multipart = {
  headers: {
    "content-type": "multipart/form-data",
    "Cache-Control": "no-cache, no-store, must-revalidate"
  }
};

export const API = {
  add: (route, data) => axios.post(`${API_ROOT}/${route}`, data, multipart),
  remove: (route, id) => axios.delete(`${API_ROOT}/${route}/${id}`),
  activate: id => axios.put(`${API_ROOT}/photos/activate/` + id),
  process: id => axios.put(`${API_ROOT}/photos/process/` + id),
  getData: type => axios.get("/" + type),
  login: credentials => axios.post(`${API_ROOT}/login`, credentials),
  logout: () => axios.post(`${API_ROOT}/logout`),
  user: () => axios.get(`${API_ROOT}/user`, multipart)
};
