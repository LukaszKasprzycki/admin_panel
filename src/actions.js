/**
 * Created by lukasz on 21.02.18.
 */
import axios from "axios";
export function add(item, panelName) {
  return {
    type: `ADD_${panelName.toUpperCase()}`,
    payload: item
  };
}
export function remove(id, panelName) {
  return {
    type: `REMOVE_${panelName.toUpperCase()}`,
    payload: id
  };
}

export function activate(id) {
  return {
    type: `ACTIVATE`,
    payload: id
  };
}
export function process(id) {
  return {
    type: `PROCESS`,
    payload: id
  };
}

export function fetch(urls = ["FRAMES", "PHOTOS", "TEXTURES"]) {
  return function action(dispatch) {
    for (let url of urls) {
      axios
        .get(`/api/${url}/`)
        .then(response => {
          dispatch(storeToRedux(response.data, url));
        })
        .catch(error => {
          console.log("ERROR", error); //better version??
        });
    }
  };
}

export function storeToRedux(response, url) {
  return {
    type: `FETCH_${url}`,
    payload: response
  };
}
