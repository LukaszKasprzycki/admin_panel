/**
 * Created by lukasz on 21.02.18.
 */
const textures = (state = [], action) => {
  switch (action.type) {
    case "FETCH_TEXTURES": {
      return [...action.payload];
    }
    case "ADD_TEXTURES": {
      return [...state, action.payload];
    }
    case "REMOVE_TEXTURES": {
      return state.filter(texture => {
        if (texture.id !== action.payload) return texture;
      });
    }
    default:
      return state;
  }
};
export default textures;
