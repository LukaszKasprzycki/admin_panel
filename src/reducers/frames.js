/**
 * Created by lukasz on 21.02.18.
 */
const frames=(state=[],action)=>{
    switch (action.type){
        case 'FETCH_FRAMES':{
            return [...action.payload]
        }
        case 'ADD_FRAMES':{
            return [...state,action.payload]
        }
        case 'REMOVE_FRAMES':{
            return state.filter(frame => {
                if (frame.id !== action.payload) return frame;
            });
        }
        default:return state;

    }
};
export default frames;