export const item = {
  id: 0,
  name: "default",
  createdAt: "2018-05-08 0:00:00",
  image: "uploads/examples/default.jpg",
  value: "uploads/examples/default.jpg"
};
export const data = [
  {
    id: 1,
    name: "example",
    createdAt: "2018-05-08 17:23:38",
    image: "uploads/examples/example.jpg",
    value: "uploads/examples/example.jpg"
  },
  {
    id: 2,
    name: "example2",
    createdAt: "2018-05-08 18:23:38",
    image: "uploads/examples/example1.jpg",
    value: "uploads/examples/example1.jpg"
  }
];

export const photosData = [
  {
    createdAt: "2018-05-07 18:26:59",
    data:
      '{"acc": {"x": null,"y": null,"z": null},"angle": {"alpha": null,"beta": null,"gamma": null}}',
    id: 0,
    image: "uploads/photos/photo.png",
    isActive: 0,
    name: "photo",
    output: "259 194",
    processed: 0,
    value: "uploads/photos/photo.png"
  },
  {
    createdAt: "2018-05-07 18:26:59",
    data:
      '{"acc": {"x": null,"y": null,"z": null},"angle": {"alpha": null,"beta": null,"gamma": null}}',
    id: 1,
    image: "uploads/photos/photo1.png",
    isActive: 0,
    name: "photo1",
    output: "259 194",
    processed: 0,
    value: "uploads/photos/photo1.png"
  },
  {
    createdAt: "2018-05-07 18:26:59",
    data:
      '{"acc": {"x": null,"y": null,"z": null},"angle": {"alpha": null,"beta": null,"gamma": null}}',
    id: 2,
    image: "uploads/photos/photo2.png",
    isActive: 1,
    name: "photo2",
    output: "259 194",
    processed: 1,
    value: "uploads/photos/photo2.png"
  }
];

export const activatedPhotos = [
  {
    createdAt: "2018-05-07 18:26:59",
    data:
      '{"acc": {"x": null,"y": null,"z": null},"angle": {"alpha": null,"beta": null,"gamma": null}}',
    id: 0,
    image: "uploads/photos/photo.png",
    isActive: 1,
    name: "photo",
    output: "259 194",
    processed: 0,
    value: "uploads/photos/photo.png"
  },
  {
    createdAt: "2018-05-07 18:26:59",
    data:
      '{"acc": {"x": null,"y": null,"z": null},"angle": {"alpha": null,"beta": null,"gamma": null}}',
    id: 1,
    image: "uploads/photos/photo1.png",
    isActive: 0,
    name: "photo1",
    output: "259 194",
    processed: 0,
    value: "uploads/photos/photo1.png"
  },
  {
    createdAt: "2018-05-07 18:26:59",
    data:
      '{"acc": {"x": null,"y": null,"z": null},"angle": {"alpha": null,"beta": null,"gamma": null}}',
    id: 2,
    image: "uploads/photos/photo2.png",
    isActive: 0,
    name: "photo2",
    output: "259 194",
    processed: 1,
    value: "uploads/photos/photo2.png"
  }
];
export const processedPhotos = [
  {
    createdAt: "2018-05-07 18:26:59",
    data:
      '{"acc": {"x": null,"y": null,"z": null},"angle": {"alpha": null,"beta": null,"gamma": null}}',
    id: 0,
    image: "uploads/photos/photo.png",
    isActive: 1,
    name: "photo",
    output: "259 194",
    processed: 0,
    value: "uploads/photos/photo.png"
  },
  {
    createdAt: "2018-05-07 18:26:59",
    data:
      '{"acc": {"x": null,"y": null,"z": null},"angle": {"alpha": null,"beta": null,"gamma": null}}',
    id: 1,
    image: "uploads/photos/photo1.png",
    isActive: 0,
    name: "photo1",
    output: "259 194",
    processed: 1,
    value: "uploads/photos/photo1.png"
  },
  {
    createdAt: "2018-05-07 18:26:59",
    data:
      '{"acc": {"x": null,"y": null,"z": null},"angle": {"alpha": null,"beta": null,"gamma": null}}',
    id: 2,
    image: "uploads/photos/photo2.png",
    isActive: 0,
    name: "photo2",
    output: "259 194",
    processed: 1,
    value: "uploads/photos/photo2.png"
  }
];
export const testScript = {
  id: 0,
  lastModified: 1516362771454,
  lastModifiedDate: "Fri Jan 19 2018 12:52:51",
  name: "example.sh",
  preview: "blob:http://localhost:3001/example",
  size: 1000,
  type: "application/x-shellscript"
};
