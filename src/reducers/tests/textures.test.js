import textures from "../textures";
import { data, item } from "./testsData";

describe("textures reducer", () => {
  test("should return the initial state", () => {
    expect(textures([], {})).toEqual([]);
  });
  test("should handle FETCH_TEXTURES", () => {
    const exampleAction = {
      type: "FETCH_TEXTURES",
      payload: data
    };
    expect(textures([], exampleAction)).toEqual(data);
  });
  test("should handle ADD_TEXTURES", () => {
    const exampleAction = {
      type: "ADD_TEXTURES",
      payload: item
    };
    expect(textures(data, exampleAction)).toEqual([...data, item]);
  });
  test("should handle REMOVE_TEXTURES", () => {
    const exampleAction = {
      type: "REMOVE_TEXTURES",
      payload: 0
    };
    expect(textures([item], exampleAction)).toEqual([]);
  });
});
