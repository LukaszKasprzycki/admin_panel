import photos from "../photos";
import {
  data,
  item,
  photosData,
  activatedPhotos,
  processedPhotos
} from "./testsData";

describe("photos reducer", () => {
  test("should return the initial state", () => {
    expect(photos([], {})).toEqual([]);
  });
  test("should handle FETCH_PHOTOS", () => {
    const exampleAction = {
      type: "FETCH_PHOTOS",
      payload: photosData
    };
    expect(photos([], exampleAction)).toEqual(photosData);
  });
  test("should handle REMOVE_PHOTOS", () => {
    const exampleAction = {
      type: "REMOVE_PHOTOS",
      payload: 0
    };
    expect(photos(photosData, exampleAction)).toEqual(photosData.slice(1));
  });
  test("should handle ACTIVATE", () => {
    const exampleAction = {
      type: "ACTIVATE",
      payload: 0
    };
    expect(photos(photosData, exampleAction)).toEqual(activatedPhotos);
  });
  test("should handle PROCESS", () => {
    const exampleAction = {
      type: "PROCESS",
      payload: 1
    };
    expect(photos(photosData, exampleAction)).toEqual(processedPhotos);
  });
});
