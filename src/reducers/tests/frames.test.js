import frames from "../frames";
import { data, item } from "./testsData";
describe("frames reducer", () => {
  test("should return the initial state", () => {
    expect(frames([], {})).toEqual([]);
  });
  test("should handle FETCH_FRAMES", () => {
    const exampleAction = {
      type: "FETCH_FRAMES",
      payload: data
    };
    expect(frames([], exampleAction)).toEqual(data);
  });
  test("should handle ADD_FRAMES", () => {
    const exampleAction = {
      type: "ADD_FRAMES",
      payload: item
    };
    expect(frames(data, exampleAction)).toEqual([...data, item]);
  });
  test("should handle REMOVE_FRAMES", () => {
    const exampleAction = {
      type: "REMOVE_FRAMES",
      payload: 0
    };
    expect(frames([item], exampleAction)).toEqual([]);
  });
});
