import script from "../script";
import { testScript } from "./testsData";

describe("script reducer", () => {
  test("should return the initial state", () => {
    expect(script(null, {})).toBeNull();
  });
  test("should handle ADD_SCRIPT", () => {
    const exampleAction = {
      type: "ADD_SCRIPT",
      payload: testScript
    };
    expect(script(null, exampleAction)).toEqual(testScript);
  });
});
