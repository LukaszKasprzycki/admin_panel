/**
 * Created by lukasz on 16.02.18.
 */
import { combineReducers } from "redux";
import textures from "./textures";
import frames from "./frames";
import photos from "./photos";
import script from "./script";

export default combineReducers({
    textures,
    frames,
    photos,
    script,
});