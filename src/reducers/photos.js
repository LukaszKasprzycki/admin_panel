/**
 * Created by lukasz on 21.02.18.
 */
const photos = (state = [], action) => {
  switch (action.type) {
    case "FETCH_PHOTOS": {
      return [...action.payload];
    }
    case "REMOVE_PHOTOS": {
      return state.filter(photo => {
        if (photo.id !== action.payload) return photo;
      });
    }
    case "ACTIVATE": {
      return state.map(photo => {
        if (photo.id === action.payload) {
          photo.isActive = 1;
          return photo;
        } else {
          photo.isActive = 0;
          return photo;
        }
      });
    }
    case "PROCESS": {
      return state.map(photo => {
        if (photo.id === action.payload) {
          photo.processed = 1;
          return photo;
        } else {
          return photo;
        }
      });
    }

    default:
      return state;
  }
};
export default photos;
