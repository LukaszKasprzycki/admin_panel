/**
 * Created by lukasz on 19.02.18.
 */
import React, { Component } from "react";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import EnhancedTableHead from "./EnhancedTableHead";
import PanelTableBody from "./PanelTableBody";
import ResultDialog from "../Elements/dialogs/ResultDialog";
import PanelTableFooter from "./PanelTableFooter";
import Table from "material-ui/Table";

const styles = theme => ({
  table: {
    overflow: "auto"
  },
  wrapper: {
    flex: "0 0 78.5%",
    overflowX: "auto",
    marginLeft: "2.5%"
  }
});
/**
 * Render Table and ResultDialogs
 */
class TableContent extends Component {
  /**
   * Creates default PanelBody state
   * @prop {number} rowsPerPage specify number of rows rendered per page
   * @prop {number} page current Page
   * @prop {string} order asceding or descending order
   */
  constructor(props) {
    super(props);
    this.state = {
      rowsPerPage: 10,
      page: 0,
      order: "asc",
      orderBy: "name",
      error: false,
      success: false
    };
  }
  /**
   * Sort column by given value
   * @public
   * @param {SyntheticEvent} event reference to event
   * @param {string} property sorting cryterium
   */
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }
    order === "desc"
      ? this.props.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
      : this.props.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ order, orderBy });
  };
  /**
   * Change number of rows rendred per page
   * @public
   * @param {SyntheticEvent} event reference to event
   * @param {string} property sorting cryterium
   */
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };
  /**
   * Change current page
   * @public
   * @param {SyntheticEvent} event reference to event
   * @param {number} page selected page
   */
  handleChangePage = (event, page) => {
    this.setState({ page });
  };
  /**
   * Control  ResultDiaglogs
   * @param {string} result success or error property of state
   *
   */
  handleResult = result => {
    this.setState({ [result]: true });
    setTimeout(() => this.setState({ [result]: false }), 1000);
  };

  render() {
    const { classes, data, tableHead, name } = this.props;
    const { order, orderBy, page, rowsPerPage, error, success } = this.state;
    /**
     * @return {ReactElement} TableContent
     */
    return (
      <div className={classes.wrapper}>
        <Table className={classes.table}>
          <EnhancedTableHead
            tableHead={tableHead}
            onRequestSort={this.handleRequestSort}
          />
          <PanelTableBody
            name={name}
            tableHead={tableHead}
            data={data}
            page={page}
            rowsPerPage={rowsPerPage}
            order={order}
            orderBy={orderBy}
            handleResult={this.handleResult}
          />
          <PanelTableFooter
            colSpan={tableHead.length}
            page={page}
            rowsPerPage={rowsPerPage}
            count={data.length}
            handleChangePage={this.handleChangePage}
            handleChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Table>
        <ResultDialog type="success" open={success} />
        <ResultDialog type="error" open={error} />
      </div>
    );
  }
}
TableContent.propTypes = {
  /**
   *Array of objects in shape:
   */
  data: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.number,
      name: propTypes.string,
      createdAt: propTypes.string,
      image: propTypes.string,
      value: propTypes.string
    })
  ),
  /**
   * Specify which type of data (textures,frames,script) is added to Redux
   */
  name: propTypes.string,
  /**
   * Array of objects in shape:
   */
  tableHead: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.string,
      label: propTypes.string,
      sortable: propTypes.bool
    })
  )
};
export default withStyles(styles)(TableContent);
