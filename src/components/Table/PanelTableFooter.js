/**
 * Created by lukasz on 19.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import { TableFooter, TablePagination, TableRow } from "material-ui/Table";
/**
 * Specify number of rows per page,change current page
 */
const PanelTableFooter = ({
  colSpan,
  count,
  rowsPerPage,
  page,
  handleChangePage,
  handleChangeRowsPerPage
}) => {
  return (
    <TableFooter style={{ marginTop: 10 }}>
      <TableRow>
        <TablePagination
          colSpan={colSpan}
          count={count}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page"
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page"
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </TableRow>
    </TableFooter>
  );
};
PanelTableFooter.propTypes = {
  /**
   * Number of columns
   */
  colSpan: propTypes.number,
  /**
   * Number of rows
   */
  count: propTypes.number,
  /**
   * Current page
   */
  page: propTypes.number,
  /**
   * Specify number of rows rendered per page
   */
  rowsPerPage: propTypes.number,
  /**
   * Change current page
   */
  handleChangePage: propTypes.func,
  /**
   * Change number of rows rendered per page
   */
  handleChangeRowsPerPage: propTypes.func
};
export default PanelTableFooter;
