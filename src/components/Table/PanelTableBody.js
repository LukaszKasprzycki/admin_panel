/**
 * Created by lukasz on 19.02.18.
 */
import React, { Component, Fragment } from "react";
import propTypes from "prop-types";
import { connect } from "react-redux";
import { TableBody, TableRow } from "material-ui/Table";
import { Delete, Settings, Save } from "material-ui-icons";
import { CircularProgress } from "material-ui/Progress";
import { remove, activate, process } from "./../../actions";
import IconCell from "./cells/IconCell";
import ImageCell from "./cells/ImageCell";
import TextCell from "./cells/TextCell";
import StatusCell from "./cells/StatusCell";
import { formatDate } from "./../../helpers";
import ConfirmDialog from "../Elements/dialogs/ConfirmDialog";
import { blue, orange, blueGrey, purple } from "material-ui/colors";
import axios from "axios";
/**
 * Contains all table data.Use:
 * @see https://github.com/axios/axios
 * @requires axios
 */
class PanelTableBody extends Component {
  /**
   * Creates default PanelTablebody state
   * @param {boolean} confirm show/hide ConfirmDialog
   * @param {undefined|number} id selected items id
   * @param {number} processed processed items id
   * @param {undefined|number} deleteId items to remove id
   */
  constructor(props) {
    super(props);
    this.state = {
      confirm: false,
      id: undefined,
      processed: -1,
      deleteId: undefined
    };
    this.showImage = this.showImage.bind(this);
    this.processPhoto = this.processPhoto.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.activate = this.activate.bind(this);
    this.saveJSON = this.saveJSON.bind(this);
    this.confirmDialog = this.confirmDialog.bind(this);
  }
  /**
   * Function show image with selected id
   * @public
   * @param {number} id selected items id
   */
  showImage(id) {
    this.setState({ id });
  }
  /**
   * Function process item with selected id
   * @public
   * @param {number} processed processed items id
   */
  processPhoto(processed) {
    this.setState({ processed });
    axios
      .put("/api/photos/process/" + processed)
      .then(response => {
        if (response.status === 201) this.props.process(processed);
        setTimeout(() => this.setState({ processed: -1 }), 1000);
      })
      .catch(error => {
        this.props.handleResult("error");
        this.setState({ processed: -1 });
      });
  }
  /**
   * Function confirm item removal
   * @public
   * @param {number} deleteId removed items id
   */
  confirmDialog(deleteId) {
    this.setState({
      confirm: !this.state.confirm,
      deleteId
    });
  }
  /**
   * Function remove item from the table
   * @public
   * @param {string} title panels name
   */
  removeItem(title) {
    axios
      .delete(`/api/${this.props.name.toLowerCase()}/${this.state.deleteId}`)
      .then(response => {
        if (response.status === 200) {
          //after successful request item is removed also from Redux
          this.props.remove(this.state.deleteId, title);
          this.props.handleResult("success");
          this.setState({ confirm: !this.state.confirm });
        }
      })
      .catch(error => {
        this.props.handleResult("error");
      });
  }
  /**
   * Function activate selected item
   * @public
   * @param {number} id items id
   */
  activate(id) {
    axios
      .put("/api/photos/activate/" + id)
      .then(response => {
        if (response.status === 201) {
          this.props.activate(id);
        }
      })
      .catch(error => {
        this.props.handleResult("error");
      });
  }
  /**
   * Function save text file to JSON
   * @public
   * @param {string} filename file name
   * @param {Object} data file content
   */
  saveJSON(filename, data) {
    const element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:application/json;charset=utf-8," + encodeURIComponent(data)
    );
    element.setAttribute("download", filename + ".json");
    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  render() {
    const { data, page, rowsPerPage, tableHead, name } = this.props;
    const { id, processed, confirm } = this.state;
    return (
      <Fragment>
        <TableBody>
          {data
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map(n => {
              return (
                <TableRow hover key={n.id}>
                  {tableHead.map((column, key) => {
                    switch (column.id) {
                      case "delete": {
                        return (
                          <IconCell
                            key={key}
                            onClick={() => this.confirmDialog(n.id)}
                          >
                            <Delete color="secondary" />
                          </IconCell>
                        );
                      }
                      case "process": {
                        return (
                          <IconCell
                            key={key}
                            onClick={() => this.processPhoto(n.id)}
                          >
                            {n.id !== processed ? (
                              <Settings style={{ color: blueGrey[600] }} />
                            ) : (
                              <CircularProgress
                                style={{ color: orange[700] }}
                              />
                            )}
                          </IconCell>
                        );
                      }
                      case "json": {
                        return (
                          <IconCell
                            key={key}
                            onClick={() => this.saveJSON(n.name, n.data)}
                          >
                            <Save style={{ color: blue[800] }} />
                          </IconCell>
                        );
                      }
                      case "activate": {
                        return (
                          <IconCell
                            color={purple[800]}
                            key={key}
                            icon={false}
                            onClick={() => this.activate(n.id)}
                          >
                            Activate
                          </IconCell>
                        );
                      }
                      case "status": {
                        return (
                          <StatusCell
                            key={key}
                            text="ready"
                            active={n.processed}
                            def={n.isActive}
                          />
                        );
                      }
                      case "createdAt": {
                        return (
                          <TextCell key={key} text={formatDate(n[column.id])} />
                        );
                      }
                      case "image": {
                        return (
                          <ImageCell
                            key={key}
                            src={n[column.id]}
                            showImage={() => this.showImage(n.id)}
                            hideImage={() => this.showImage(0)}
                            id={id}
                            hovered={n.id}
                          />
                        );
                      }
                      default:
                        return <TextCell key={key} text={n[column.id]} />;
                    }
                  })}
                </TableRow>
              );
            })}
        </TableBody>
        <ConfirmDialog
          open={confirm}
          close={() => this.confirmDialog(undefined)}
          deleteItem={() => this.removeItem(name)}
        />
      </Fragment>
    );
  }
}
PanelTableBody.propTypes = {
  /**
   * Array of objects in shape:
   */
  data: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.number,
      name: propTypes.string,
      createdAt: propTypes.string,
      image: propTypes.string,
      value: propTypes.string
    })
  ),
  /**
   * Specify which type of data (textures,frames,script) is added to Redux
   */
  name: propTypes.string,
  /**
   * Sorting order
   */
  order: propTypes.string,
  /**
   * Column to sort
   */
  orderBy: propTypes.string,
  /**
   * Activate item in Redux
   */
  activate: propTypes.func,
  /**
   * Process item in Redux
   */
  process: propTypes.func,
  /**
   * Remove item from Redux
   */
  remove: propTypes.func,
  /**
   * Specify number of rows rendered per page
   */
  rowsPerPage: propTypes.number,
  /**
   * Array of objects in shape:
   */
  tableHead: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.string,
      label: propTypes.string,
      sortable: propTypes.bool
    })
  )
};
export default connect(null, { remove, activate, process })(PanelTableBody);
