/**
 * Created by lukasz on 19.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel
} from "material-ui/Table";
import Tooltip from "material-ui/Tooltip";
import { withStyles } from "material-ui/styles";

const styles = theme => ({
  cell: {
    fontWeight: "bold",
    padding: 5,
    textAlign: "center",
    fontSize: 14,
    color: "black",
    [theme.breakpoints.down("md")]: {
      padding: 5
    }
  }
});
/**
 * TableHead with ability to sort
 */
const EnhancedTableHead = ({ classes, tableHead, onRequestSort }) => {
  /**
   * Function responsible for sorting by sortable value
   * @public
   * @param {string} property sorting criteria
   */
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };
  return (
    <TableHead>
      <TableRow>
        {tableHead.map(column => {
          return column.sortable ? (
            <TableCell key={column.id} className={classes.cell}>
              <Tooltip title="Sort" enterDelay={200}>
                <TableSortLabel
                  active={false}
                  onClick={createSortHandler(column.id)}
                >
                  {column.label}
                </TableSortLabel>
              </Tooltip>
            </TableCell>
          ) : (
            <TableCell key={column.id} numeric={true} className={classes.cell}>
              {column.label}
            </TableCell>
          );
        })}
      </TableRow>
    </TableHead>
  );
};
EnhancedTableHead.propTypes = {
  /**
   * Function responsible for sorting
   */
  onRequestSort: propTypes.func,
  /**
   Array of objects in shape:
   */
  tableHead: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.string,
      label: propTypes.string,
      sortable: propTypes.bool
    })
  )
};
export default withStyles(styles)(EnhancedTableHead);
