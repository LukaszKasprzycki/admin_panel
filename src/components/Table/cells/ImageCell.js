/**
 * Created by lukasz on 26.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import { TableCell } from "material-ui/Table";
import { withStyles } from "material-ui/styles";
const styles = theme => ({
  cell: {
    textAlign: "center",
    position: "relative",
    padding: 5
  },
  smallImage: {
    width: 45,
    height: 45,
    margin: [3, 0],
    borderRadius: "3px"
  },
  bigImage: {
    position: "absolute",
    top: "5px",
    right: `calc(50% - ${150}px)`,
    height: 175,
    width: 175,
    zIndex: 1,
    borderRadius: 4,
    overflow: "scroll"
  }
});
const ImageCell = ({
  classes,
  src,
  hideImage,
  showImage,
  id,
  hovered,
  key
}) => {
  return (
    <TableCell key={key} className={classes.cell}>
      <img
        className={id === hovered ? classes.bigImage : classes.smallImage}
        onMouseEnter={() => showImage()}
        onMouseLeave={() => hideImage(0)}
        src={src}
        alt=""
      />
    </TableCell>
  );
};
ImageCell.propTypes = {
  /**
   * Source to image.
   */
  src: propTypes.string,
  /**
   * Function hide image on blur
   */
  hideImage: propTypes.func,
  /**
   * Function show image on hover
   */
  showImage: propTypes.func,
  /**
   * Id of image cell
   */
  id: propTypes.number,
  /**
   * Id of hovered image cell
   */
  hovered: propTypes.number,
  /**
   * Key provided to identify ImageCell
   */
  key: propTypes.number
};
export default withStyles(styles)(ImageCell);
