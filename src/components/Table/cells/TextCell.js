/**
 * Created by lukasz on 26.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import { TableCell } from "material-ui/Table";
import { withStyles } from "material-ui/styles";
const styles = theme => ({
  cell: {
    textAlign: "center",
    position: "relative",
    maxWidth: 80,
    overflow: "hidden"
  }
});
const TextCell = ({ classes, text }) => {
  return <TableCell className={classes.cell}>{text}</TableCell>;
};
TextCell.propTypes = {
  /**
   * Text provided into TextCell
   */
  text: propTypes.string
};
export default withStyles(styles)(TextCell);
