/**
 * Created by lukasz on 26.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import { TableCell } from "material-ui/Table";
import { withStyles } from "material-ui/styles";
import IconButton from "material-ui/IconButton";
import Typography from "material-ui/Typography";
const styles = theme => ({
  cell: {
    textAlign: "center",
    position: "relative",
    padding: 5
  },
  iconCell: {
    margin: 0,
    padding: 0
  },
  textCell: {
    fontSize: 14
  }
});
const IconCell = ({ children, classes, onClick, icon, key, color }) => {
  return (
    <TableCell
      key={key}
      className={classes.cell}
      onClick={argument => onClick(argument)}
    >
      <IconButton className={classes.iconCell}>
        {icon ? (
          children
        ) : (
          <Typography className={classes.textCell} style={{ color }}>
            {children}
          </Typography>
        )}
      </IconButton>
    </TableCell>
  );
};

IconCell.defaultProps = {
  color: "primary",
  icon: true
};
IconCell.propTypes = {
  /**
   * Content of cell.Icon or text
   */
  children: propTypes.oneOfType([propTypes.object, propTypes.string]),
  /**
   * Function provided in PanelTableBody
   */
  onClick: propTypes.func,
  /**
   * Specify if IconCell will contain icon or text
   */
  icon: propTypes.bool,
  /**
   * Key provided to identify IconCell
   */
  key: propTypes.number,
  /**
   * Color of icon/text
   */
  color: propTypes.string
};
export default withStyles(styles)(IconCell);
