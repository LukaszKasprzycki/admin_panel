/**
 * Created by lukasz on 05.03.18.
 */
import React from "react";
import propTypes from "prop-types";
import { TableCell } from "material-ui/Table";
import { withStyles } from "material-ui/styles";
import Chip from "material-ui/Chip";
import DoneIcon from "material-ui-icons/Done";
import { green } from "material-ui/colors";
const styles = theme => ({
  cell: {
    textAlign: "left",
    position: "relative",
    padding: 5
  },
  active: {
    backgroundColor: green[300],
    color: "white",
    pointerEvents: "none"
  },
  default: {
    fontWeight: "bold",
    marginLeft: 5
  },
  icon: {
    pointerEvents: "none",
    color: "white"
  },
  hide: {
    display: "none"
  }
});
const StatusCell = ({ classes, active, def }) => {
  return (
    <TableCell className={classes.cell}>
      <Chip
        label={active ? "ready" : "raw"}
        deleteIcon={
          <DoneIcon className={active ? classes.icon : classes.hide} />
        }
        onDelete={() => {}}
        className={active ? classes.active : classes.default}
      />
      {def ? <Chip label="default" className={classes.default} /> : null}
    </TableCell>
  );
};
StatusCell.propTypes = {
  /**
   * Specify if StatusCell is active
   */
  active: propTypes.number,
  /**
   * Specify if StatusCell is default
   */
  def: propTypes.number
};

export default withStyles(styles)(StatusCell);
