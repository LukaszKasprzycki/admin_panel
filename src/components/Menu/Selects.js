/**
 * Created by lukasz on 07.03.18.
 */
import React, { Fragment } from "react";
import compose from "recompose/compose";
import propTypes from "prop-types";
import withWidth from "material-ui/utils/withWidth";
import DesktopWrapper from "./Wrappers/DesktopWrapper";
import MobileWrapper from "./Wrappers/MobileWrapper";
import SelectBoxes from "./Children/SelectBoxes";
/**
 * Uses:
 * @see https://github.com/acdlite/recompose
 * @requires recompose
 */
const Selects = ({ select1, select2, select3, onChange, width }) => {
  /**
   * @constant bigScreen contains bigScrenn width
   * @see {@link{https://material-ui-next.com/layout/hidden/}}
   */
  const bigScreen = ["lg", "xl"];
  const header = "Select";
  return (
    <Fragment>
      {bigScreen.includes(width) ? (
        <DesktopWrapper header={header}>
          <SelectBoxes
            select1={select1}
            select2={select2}
            select3={select3}
            onChange={onChange}
          />
        </DesktopWrapper>
      ) : (
        <MobileWrapper header={header}>
          <SelectBoxes
            select1={select1}
            select2={select2}
            select3={select3}
            onChange={onChange}
          />
        </MobileWrapper>
      )}
    </Fragment>
  );
};

Selects.propTypes = {
  /**
   * Function changes selected value
   */
  onChange: propTypes.func,
  /**
   * Option 1
   */
  select1: propTypes.bool,
  /**
   * Option 2
   */
  select2: propTypes.bool,
  /**
   * Option 3
   */
  select3: propTypes.bool,
  /**
   * Current screen width
   */
  width: propTypes.string
};
export default compose(withWidth())(Selects);
