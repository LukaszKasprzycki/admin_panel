/**
 * Created by lukasz on 07.03.18.
 */
import React from "react";
import propTypes from "prop-types";
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails
} from "material-ui/ExpansionPanel";
import { withStyles } from "material-ui/styles";
import Typography from "material-ui/Typography";
import withWidth from "material-ui/utils/withWidth";
import compose from "recompose/compose";
import ExpandMoreIcon from "material-ui-icons/ExpandMore";
import { grey } from "material-ui/colors";

const styles = theme => ({
  panel: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    minHeight: 54
  },
  header: {
    fontWeight: "bold",
    fontSize: 14,
    fontFamily: "sans",
    cursor: "default"
  },
  root: {
    border: "2px solid white",
    marginBottom: 5,
    backgroundColor: grey[50],
    "&:before": {
      opacity: 0
    }
  }
});
/**
 * RadioButtons and SelectBoxes wrapper for small screen.Uses:
 * @see https://github.com/acdlite/recompose
 * @requires recompose
 */

const MobileWrapper = ({ classes, header, children }) => {
  return (
    <ExpansionPanel
      className={classes.panel}
      elevation={0}
      classes={{
        root: classes.root
      }}
    >
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.header}>{header}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>{children}</ExpansionPanelDetails>
    </ExpansionPanel>
  );
};
MobileWrapper.propTypes = {
  /**
   * Container header
   */
  header: propTypes.string,
  /**
   * RadioButtons or SelectBoxes
   */
  children: propTypes.element
};
export default compose(withStyles(styles), withWidth())(MobileWrapper);
