/**
 * Created by lukasz on 19.03.18.
 */
import React, { Fragment } from "react";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
const styles = theme => ({
  container: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  header: {
    height: "54px",
    display: "flex",
    justifyContent: "center"
  },
  text: {
    fontWeight: "bold",
    fontSize: 14,
    fontFamily: "sans-serif",
    alignSelf: "center"
  }
});
/**
 *RadioButtons and SelectBoxes wrapper for large screen
 */
const DesktopWrapper = ({ classes, children, header }) => {
  return (
    <Fragment>
      <div className={classes.container}>
        <span className={classes.text}>{header}</span>
      </div>
      <div className={classes.container}>{children}</div>
    </Fragment>
  );
};
DesktopWrapper.propTypes = {
  /**
   * RadioButtons or SelectBoxes
   */
  children: propTypes.element,
  /**
   * Container header
   */
  header: propTypes.string
};
export default withStyles(styles)(DesktopWrapper);
