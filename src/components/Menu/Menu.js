/**
 * Created by lukasz on 19.02.18.
 */
import React, { Component } from "react";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Filters from "./Filters";
import Selects from "./Selects";
import Inputs from "./Inputs";
import { grey } from "material-ui/colors";
const styles = theme => ({
  menu: {
    marginTop: 5,
    backgroundColor: grey[50],
    width: "100%",
    position: "relative",
    padding: 0,
    height: "auto",
    [theme.breakpoints.up("md")]: {
      order: 1
    }
  }
});
/**
 * Menu Container.Contains Input,Filters and Selects
 */
class Menu extends Component {
  //temporary solution,waiting for more data
  /** Default state of Menu
   * @constructor
   * @param {string} value value of selected filter,
   * @param {boolean} select1 specify if first checkbox is selected
   * @param {boolean} select2 specify if second checkbox is selected
   * @param {boolean} select3 specify if third checkbox is selected
   */
  state = {
    value: "filter1",
    select1: false,
    select2: false,
    select3: false
  };
  /**
   * Function change selected filter value
   * @public
   * @param {SyntheticEvent} event event object
   * @param {string} value value of selected filter
   */
  handleChange = (event, value) => {
    this.setState({ value });
  };
  /**
   * Function selects selectbox
   * @public
   * @param {string} name selected checkbox
   */
  handleSelect = name => (event, checked) => {
    this.setState({ [name]: checked });
  };

  render() {
    const { classes, handleChange, input } = this.props;
    const { value, select1, select2, select3 } = this.state;
    /**
     * @constant filters example filters data
     */
    const filters = [
      { value: "filter1", label: "Filter 1" },
      { value: "filter2", label: "Filter 2" },
      { value: "filter3", label: "Filter 3" }
    ];
    return (
      <div className={classes.menu}>
        <Inputs input={input} onChange={handleChange} />
        <Filters value={value} filters={filters} onChange={this.handleChange} />
        <Selects
          select1={select1}
          select2={select2}
          select3={select3}
          onChange={this.handleSelect}
        />
      </div>
    );
  }
}
Menu.propTypes = {
  /**
   * Function change select filter
   */
  handleChange: propTypes.func,
  /**
   * Input provided by user
   */
  input: propTypes.string
};
export default withStyles(styles)(Menu);
