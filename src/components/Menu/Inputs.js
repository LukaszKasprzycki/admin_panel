import React, { Fragment } from "react";
import compose from "recompose/compose";
import propTypes from "prop-types";
import withWidth from "material-ui/utils/withWidth";
import Header from "./Children/Header";
import Input from "./Children/Input";
/**
 * Wrapper for Input.Uses:
 * @see https://github.com/acdlite/recompose
 * @requires recompose
 */
const Inputs = ({ input, onChange, width }) => {
  const bigScreen = ["lg", "xl"];
  const header = "Search";
  return (
    <Fragment>
      {bigScreen.includes(width) ? (
        <Fragment>
          <Header header={header} />
          <Input input={input} onChange={onChange} />
        </Fragment>
      ) : (
        <Input input={input} onChange={onChange} placeholder={header} />
      )}
    </Fragment>
  );
};
Inputs.propTypes = {
  /**
   * User input
   */
  input: propTypes.string,
  /**
   * Searching function
   */
  onChange: propTypes.func,
  /**
   * Current screen width
   */
  width: propTypes.string
};
export default compose(withWidth())(Inputs);
