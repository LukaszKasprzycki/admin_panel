/**
 * Created by lukasz on 19.03.18.
 */
import React from "react";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import { blue } from "material-ui/colors";

const styles = theme => ({
  container: {
    height: "54px",
    display: "flex",
    justifyContent: "center"
  },
  searchField: {
    width: "80%",
    border: `1px solid ${blue[100]}`,
    height: "50%",
    borderRadius: 3,
    fontSize: 15,
    paddingLeft: 10,
    alignSelf: "center"
  }
});
/**
 * Menu input field
 */
const Input = ({ classes, input, onChange, placeholder = "" }) => {
  return (
    <div className={classes.container}>
      <input
        type="text"
        value={input}
        onChange={onChange}
        className={classes.searchField}
        placeholder={placeholder}
      />
    </div>
  );
};
Input.propTypes = {
  /**
   * Input provided by user
   */
  input: propTypes.string,
  /**
   * Searching function
   */
  onChange: propTypes.func,
  /**
   * Placeholder in Input
   */
  placeholder: propTypes.string
};
export default withStyles(styles)(Input);
