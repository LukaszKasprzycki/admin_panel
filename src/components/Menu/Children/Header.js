/**
 * Created by lukasz on 19.03.18.
 */
import React from "react";
import { withStyles } from "material-ui/styles";
import propTypes from "prop-types";

const styles = theme => ({
  container: {
    height: "54px",
    display: "flex",
    justifyContent: "center"
  },
  text: {
    fontWeight: "bold",
    fontSize: 14,
    fontFamily: "sans-serif",
    alignSelf: "center",
    paddingBottom: 5
  }
});
/**
 *Menu header
 */
const Header = ({ classes, header }) => {
  return (
    <div className={classes.container}>
      <span className={classes.text}>{header}</span>
    </div>
  );
};
Header.propTypes = {
  /**
   * Header in menu component
   */
  header: propTypes.string
};
export default withStyles(styles)(Header);
