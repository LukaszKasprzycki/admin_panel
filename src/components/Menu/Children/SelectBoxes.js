import React from "react";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import { FormGroup, FormControlLabel } from "material-ui/Form";
import Checkbox from "material-ui/Checkbox";
import purple from "material-ui/colors/purple";
const styles = theme => ({
  formControl: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  checked: {
    color: purple[800]
  },
  formGroup: {
    width: "auto",
    display: "flex",
    flexDirection: "column",
    flexWrap: "wrap",
    [theme.breakpoints.down("md")]: {
      flexDirection: "row",
      width: "auto",
      margin: 0,
      paddingLeft: 10
    }
  }
});
/**
 *Container for SelectBoxes(temporary solution)
 */
const SelectBoxes = ({ classes, select1, select2, select3, onChange }) => {
  return (
    <FormGroup className={classes.formGroup}>
      <FormControlLabel
        control={
          <Checkbox
            checked={select1}
            onChange={onChange("select1")}
            value={"Input value here in future"}
            classes={{
              checked: classes.checked
            }}
          />
        }
        label="Select 1"
      />
      <FormControlLabel
        control={
          <Checkbox
            checked={select2}
            onChange={onChange("select2")}
            value={"Input value here in future"}
            classes={{
              checked: classes.checked
            }}
          />
        }
        label="Select 2"
      />
      <FormControlLabel
        control={
          <Checkbox
            checked={select3}
            onChange={onChange("select3")}
            value={"Input value here in future"}
            classes={{
              checked: classes.checked
            }}
          />
        }
        label="Select 3"
      />
    </FormGroup>
  );
};
SelectBoxes.propTypes = {
  /**
   * option 1
   */
  select1: propTypes.bool,
  /**
   * option 2
   */
  select2: propTypes.bool,
  /**
   * option 3
   */
  select3: propTypes.bool,
  /**
   * Function change selected value
   */
  onChange: propTypes.func
};
export default withStyles(styles)(SelectBoxes);
