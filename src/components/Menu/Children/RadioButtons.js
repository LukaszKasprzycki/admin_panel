import React from "react";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import { FormControlLabel } from "material-ui/Form";
import Radio, { RadioGroup } from "material-ui/Radio";
import blue from "material-ui/colors/blue";
const styles = theme => ({
  formControl: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  radio: {
    display: "flex",
    flexDirection: "column",
    flexWrap: "wrap",
    width: "auto",
    [theme.breakpoints.down("md")]: {
      flexDirection: "row"
    }
  },
  checked: {
    color: blue[800]
  }
});
/**
 *Container for RadioButtons
 */
const RadioButtons = ({ classes, value, onChange, filters }) => {
  return (
    <RadioGroup
      aria-label="filters"
      name="filters"
      className={classes.radio}
      value={value}
      onChange={onChange}
    >
      {filters.map(filter => {
        return (
          <FormControlLabel
            value={filter.value}
            key={filter.value}
            control={
              <Radio
                classes={{
                  checked: classes.checked
                }}
              />
            }
            label={filter.label}
          />
        );
      })}
    </RadioGroup>
  );
};
RadioButtons.propTypes = {
  /**
   * Array of elements in shape
   */
  filters: propTypes.arrayOf(
    propTypes.shape({
      value: propTypes.string,
      label: propTypes.string
    })
  ),
  /**
   * Function change select filter
   */
  onChange: propTypes.func,
  /**
   * Value of selected filter
   */
  value: propTypes.string
};
export default withStyles(styles)(RadioButtons);
