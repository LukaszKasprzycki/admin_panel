/**
 * Created by lukasz on 07.03.18.
 */
import React, { Fragment } from "react";
import compose from "recompose/compose";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import withWidth from "material-ui/utils/withWidth";
import DesktopWrapper from "./Wrappers/DesktopWrapper";
import MobileWrapper from "./Wrappers/MobileWrapper";
import RadioButtons from "./Children/RadioButtons";
/**
 * Uses:
 * @see https://github.com/acdlite/recompose
 * @requires recompose
 */
const Filters = ({ value, onChange, filters, width }) => {
  /**
   * @constant bigScreen contains bigScrenn width
   * @see https://material-ui-next.com/layout/hidden/
   */
  const bigScreen = ["lg", "xl"];
  const header = "Filters";
  return (
    <Fragment>
      {bigScreen.includes(width) ? (
        <DesktopWrapper header={header}>
          <RadioButtons filters={filters} onChange={onChange} value={value} />
        </DesktopWrapper>
      ) : (
        <MobileWrapper header={header}>
          <RadioButtons filters={filters} onChange={onChange} value={value} />
        </MobileWrapper>
      )}{" "}
    </Fragment>
  );
};
Filters.propTypes = {
  /**
   * Array of elements in shape
   */
  filters: propTypes.arrayOf(
    propTypes.shape({
      value: propTypes.string,
      label: propTypes.string
    })
  ),
  /**
   * Function change select filter
   */
  onChange: propTypes.func,
  /**
   * Current screen width
   */
  width: propTypes.string,
  /**
   * Value of selected filter
   */
  value: propTypes.string
};
const styles = theme => ({});

export default compose(withStyles(styles), withWidth())(Filters);
