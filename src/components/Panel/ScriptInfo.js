/**
 * Created by lukasz on 14.03.18.
 */
import React from "react";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Typography from "material-ui/Typography";
import blue from "material-ui/colors/blue";
const styles = theme => ({
  code: {
    padding: 15,
    marginBottom: 24,
    marginLeft: 15,
    color: blue[900],
    whiteSpace: "pre-wrap"
  },
  info: {
    padding: 15
  }
});
/**
 * Contains loaded script or error message
 */
const ScriptInfo = ({ classes, script }) => {
  return (
    <div className={classes.container}>
      {script ? (
        <Typography className={classes.code}>{script.content}</Typography>
      ) : (
        <Typography color="secondary" className={classes.info}>
          sorry,there is no active script
        </Typography>
      )}
    </div>
  );
};
ScriptInfo.propTypes = {
  /**
   * Script object
   */
  script: propTypes.object
};
export default withStyles(styles)(ScriptInfo);
