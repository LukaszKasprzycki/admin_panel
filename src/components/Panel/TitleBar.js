/**
 * Created by lukasz on 26.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import AppBar from "material-ui/AppBar";
import IconButton from "material-ui/IconButton";
import { ExitToApp } from "material-ui-icons";
import MenuIcon from "material-ui-icons/Menu";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import teal from "material-ui/colors/teal";

const styles = theme => ({
  appBar: {
    color: teal[200],
    backgroundColor: "black",
    position: "absolute",
    marginLeft: 240,
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - ${250}px)`,
      display: "none"
    },
    marginBottom: 30
  },
  toolbar: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 0
  }
});
/**
 * AppBar for mobile version
 */
const TitleBar = ({ title, classes, handleDrawerToggle, logOut }) => {
  return (
    <AppBar className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleDrawerToggle}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="title" color="inherit" noWrap>
          {title}
        </Typography>

        <IconButton
          color="inherit"
          aria-label="logout"
          onClick={() => logOut()}
        >
          <ExitToApp />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

TitleBar.defaultProps = {
  title: "Admin Panel"
};

TitleBar.propTypes = {
  /**
   * Title of AppBar
   */
  title: propTypes.string,
  /**
   * Function toggle drawer
   */
  handleDrawerToggle: propTypes.func,
  /**
   * Function log user out of application
   */
  logOut: propTypes.func
};

export default withStyles(styles)(TitleBar);
