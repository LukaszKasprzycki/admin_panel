/**
 * Created by lukasz on 27.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import Typography from "material-ui/Typography";
import { withStyles } from "material-ui/styles";
import { grey, red, teal } from "material-ui/colors";
import { ExitToApp } from "material-ui-icons";
import IconButton from "material-ui/IconButton";
import Paper from "material-ui/Paper";
import Tooltip from "material-ui/Tooltip";
const styles = theme => ({
  tableHeading: {
    height: "8vh",
    fontSize: "1.5em",
    verticalAlign: "middle",
    lineHeight: "8vh",
    paddingLeft: 10,
    alignItems: "center",
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.down("md")]: {
      display: "none"
    }
  },
  current: {
    fontSize: "smaller",
    color: grey[500]
  },
  logout: {
    marginRight: 40,
    fontSize: 30
  },
  icon: {
    color: teal[500],
    borderRadius: "50%",
    fontSize: "1.2em",
    fontWeight: "bold",
    padding: 5,
    "&:hover": {
      color: red[600],
      border: "none"
    }
  }
});
/**
 * Is showed when screen is large
 */
const PanelTitle = ({ classes, name, history, logOut }) => {
  return (
    <Paper>
      <Typography variant="title" className={classes.tableHeading}>
        <span>
          admin/<span className={classes.current}>{name.toLowerCase()}</span>
        </span>
        <Tooltip title="Log out" enterDelay={300}>
          <IconButton
            variant="raised"
            color="primary"
            className={classes.logout}
            onClick={() => logOut()}
          >
            <ExitToApp className={classes.icon} />
          </IconButton>
        </Tooltip>
      </Typography>
    </Paper>
  );
};

PanelTitle.propTypes = {
  /**
   * Specify which type of data (textures,frames,script) is added to Redux
   */
  name: propTypes.string,
  /**
   * History object from React Router
   */
  history: propTypes.object,
  /**
   * Function log user out of application
   */
  logOut: propTypes.func
};

export default withStyles(styles)(PanelTitle);
