/**
 * Created by lukasz on 27.02.18.
 */
import React, { Component } from "react";
import Menu from "../Menu/Menu";
import TableContent from "../Table/TableContent";
import { withStyles } from "material-ui/styles";
import propTypes from "prop-types";
const styles = theme => ({
  content: {
    width: "100%",
    flexGrow: 1,
    alignSelf: "flex-end",
    display: "flex",
    flexDirection: "column",
    [theme.breakpoints.up("md")]: {
      flexDirection: "row"
    }
  }
});
/**
 * Contains table of items and menu
 */
class PanelBody extends Component {
  /**
   * Create default PanelBody state.
   * Contain input provided by user
   */
  constructor(props) {
    super(props);
    this.state = {
      input: ""
    };
    this.handleChange = this.handleChange.bind(this);
  }
  /**
   * Function provide user input into state
   * @public
   * @param {SyntheticEvent} event
   *
   */
  handleChange(event) {
    this.setState({ input: event.target.value });
  }

  render() {
    const { classes, name, tableHead, data } = this.props;
    return (
      <div className={classes.content}>
        <Menu
          input={this.state.input}
          handleChange={e => this.handleChange(e)}
        />
        <TableContent
          name={name}
          tableHead={tableHead}
          data={data.filter(element => {
            if (element.name.includes(this.state.input)) return element;
          })}
        />
      </div>
    );
  }
}
PanelBody.propTypes = {
  /**
   * Specify which type of data (textures,frames,script) is added to Redux
   */
  name: propTypes.string,
  /**
   * Array of columns name.
   * Constains object in shape:
   */
  tableHead: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.string,
      label: propTypes.string,
      sortable: propTypes.bool
    })
  ),
  /**
   * Array of items in table.
   * Constains object in shape:
   */
  data: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.number,
      name: propTypes.string,
      createdAt: propTypes.string,
      image: propTypes.string,
      value: propTypes.string
    })
  )
};
export default withStyles(styles)(PanelBody);
