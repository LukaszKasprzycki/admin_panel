/**
 * Created by lukasz on 19.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import DropZone from "../Dropzone/DropZone";
import PanelTitle from "./PanelTitle";
import PanelBody from "./PanelBody";
import { withStyles } from "material-ui/styles";
const styles = theme => ({
  panel: {
    display: "flex",
    flexDirection: "column",
    height: "100%"
  }
});
/**
 * Main Panel Component.Contains PanelTitle,Dropzone(if props dropzone=true) and PanelBody
 */
const Panel = ({
  name,
  data,
  tableHead,
  add,
  accept,
  dropzone,
  history,
  logOut,
  classes
}) => {
  return (
    <div className={classes.panel}>
      <PanelTitle name={name} history={history} logOut={logOut} />
      {dropzone && <DropZone name={name} add={add} accept={accept} />}
      <PanelBody name={name} tableHead={tableHead} data={data} />
    </div>
  );
};

Panel.defaultProps = {
  dropzone: true
};
Panel.propTypes = {
  /**
   * Specify if dropzone should be show
   */
  dropzone: propTypes.bool,
  /**
   * Specify which types of files can be added
   */
  accept: propTypes.string,
  /**
   * Function add item into Redux
   */
  add: propTypes.func,
  /**
   * Specify which type of data (textures,frames,script) is added into Redux
   */
  name: propTypes.string,
  /**
   * Array of objects in shape:
   */
  data: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.number,
      name: propTypes.string,
      createdAt: propTypes.string,
      image: propTypes.string,
      value: propTypes.string
    })
  ),
  /**
   * Array of objects in shape:
   */
  tableHead: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.string,
      label: propTypes.string,
      sortable: propTypes.bool
    })
  )
};

export default withStyles(styles)(Panel);
