import React from "react";
import { withStyles } from "material-ui/styles";
import TitleBar from "./Panel/TitleBar";
import DrawerWrapper from "./Drawer/DrawerWrapper";
import Content from "./Content";
import PropTypes from "prop-types";

const styles = theme => ({
  appFrame: {
    minHeight: "100vh",
    display: "flex"
  }
});
/**
 * Major AdminPanel Component
 */
class AdminPanel extends React.Component {
  /**
   *
   * @param {boolean} mobileOpen is responsive drawer hidded/showed
   */
  state = {
    mobileOpen: false
  };
  /**
   * Function toggle responsive drawer
   * @public
   */
  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };

  render() {
    const {
      classes,
      theme,
      textures,
      frames,
      photos,
      history,
      script,
      user,
      logOut
    } = this.props;
    return (
      <div className={classes.appFrame}>
        <TitleBar
          handleDrawerToggle={this.handleDrawerToggle}
          logOut={logOut}
        />
        <DrawerWrapper
          open={this.state.mobileOpen}
          user={user}
          theme={theme}
          handleDrawerToggle={this.handleDrawerToggle}
          history={history}
        />
        <Content
          textures={textures}
          frames={frames}
          photos={photos}
          script={script}
          history={history}
          logOut={logOut}
        />
      </div>
    );
  }
}
AdminPanel.propTypes = {
  /**
   *  History Object from React Router
   */
  history: PropTypes.object,
  /**
   * Function log user out of application
   */
  logOut: PropTypes.func,
  /**
   *User data.By default  undefined.
   When user is logged object in shape:
   */
  user: PropTypes.shape({
    user: PropTypes.shape({
      username: PropTypes.string,
      id: PropTypes.number
    })
  }),
  /**
   * Frames data stored in redux.By default [].
   * After data is fetched array of objects with shape:
   */
  frames: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      createdAt: PropTypes.string,
      image: PropTypes.string,
      value: PropTypes.string
    })
  ),
  /**
   * Textures data stored in redux.By default [].
   * After data is fetched array of objects with shape:
   */
  textures: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      createdAt: PropTypes.string,
      image: PropTypes.string,
      value: PropTypes.string
    })
  ),
  /**
   * Photos data stored in redux.By default [].
   * After data is fetched array of objects with shape:
   */
  photos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      createdAt: PropTypes.string,
      image: PropTypes.string,
      value: PropTypes.string
    })
  ),
  /**
   * Theme Object from Material UI.See:
   * https://material-ui-next.com/customization/themes/
   */
  theme: PropTypes.object,
  /**
   *Script object
   */
  script: PropTypes.object
};
export default withStyles(styles)(AdminPanel);
