/**
 * Created by lukasz on 22.02.18.
 */
import React from "react";
import Panel from "./Panel/Panel";
import propTypes from "prop-types";
import { add } from "../actions";
import { connect } from "react-redux";
const Frames = ({ data, history, logOut, add }) => {
  const tableHead = [
    {
      id: "image",
      label: "Image",
      sortable: false
    },
    {
      id: "name",
      label: "Name",
      sortable: true
    },
    {
      id: "createdAt",
      label: "Created",
      sortable: true
    },
    {
      id: "delete",
      label: "Delete",
      sortable: false
    }
  ];
  return (
    <Panel
      tableHead={tableHead}
      history={history}
      data={data}
      logOut={logOut}
      accept="image/jpeg, image/png, .obj"
      name="frames"
      add={add}
    />
  );
};
Frames.propTypes = {
  /**
   *Array of objects in shape:
   */
  data: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.number,
      name: propTypes.string,
      createdAt: propTypes.string,
      image: propTypes.string,
      value: propTypes.string
    })
  ),
  /**
   * History Object from React Router
   */
  history: propTypes.object,
  /**
   * Function log user out of application
   */
  logOut: propTypes.func
};
export default connect(null, { add })(Frames);
