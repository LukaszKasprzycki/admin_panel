/**
 * Created by lukasz on 22.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import Paper from "material-ui/Paper";
import DropZone from "./Dropzone/DropZone";
import { add } from "../actions";
import PanelTitle from "./Panel/PanelTitle";
import ScriptInfo from "./Panel/ScriptInfo";
import { connect } from "react-redux";

const Script = ({ script, add, history, logOut }) => {
  return (
    <Paper elevation={0}>
      <PanelTitle history={history} name="script" logOut={logOut} />
      <DropZone name="script" add={add} accept=".sh" />
      <ScriptInfo script={script} />
    </Paper>
  );
};
Script.propTypes = {
  /**
   * Script object
   */
  script: propTypes.object,
  /**
   * Add items to Redux store
   */
  add: propTypes.func,
  /**
   * History Object from React Router
   */
  history: propTypes.object,
  /**
   * Function log user out of application
   */
  logOut: propTypes.func
};
export default connect(null, { add })(Script);
