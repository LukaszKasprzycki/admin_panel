/**
 * Created by lukasz on 16.02.18.
 */
import React, { Component } from "react";
import propTypes from "prop-types";
import Dropzone from "react-dropzone";
import ListContainer from "./ListContainer";
import { withStyles } from "material-ui/styles";
import DropzoneIcon from "./DropzoneIcon";
import { isEqualArray, getExtFromName, schemas, API } from "./../../helpers";
import { lightBlue, grey } from "material-ui/colors";
import ResultDialog from "../Elements/dialogs/ResultDialog";
const styles = theme => ({
  container: {
    width: "100%",
    marginTop: 5,
    marginBottom: 5,
    position: "relative"
  },
  dropzone: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 5,
    height: 80,
    width: "98%",
    marginLeft: "1%",
    marginRight: "1%",
    paddingTop: 10,
    backgroundColor: grey[100],
    borderRadius: "1.5em",
    border: `1px dashed ${lightBlue[600]}`,
    "&:hover": {
      backgroundColor: grey[200]
    },
    [theme.breakpoints.down("md")]: {
      height: 60,
      padding: 0
    }
  }
});
/**
 * Dropzone Container.Contain all items associated with drag and drop functionality.Uses react-dropzone
 * @requires react-dropzone
 * @see https://react-dropzone.js.org/
 */
class DropZone extends Component {
  /**
   * Creates default DropZone state
   * @param {Array} accepted-array of items accepted by DropZone
   * @param {Array} rejected-array of items rejected by DropZone
   * @param {boolean} success-show(true)/hide(false) success ResultDialog
   * @param {boolean} error-show(true)/hide(false) error ResultDialog
   * @param {undefined} message-error message send to Resultdialog
   */
  constructor(props) {
    super(props);
    this.state = {
      accepted: [],
      rejected: [],
      success: false,
      error: false,
      message: undefined
    };
    this.onDrop = this.onDrop.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
    this.handleDialog = this.handleDialog.bind(this);
    this.appendData = this.appendData.bind(this);
  }
  /**
   * Function add unique id to every item in arrays of accepted and rejected items
   * @public
   * @param {Array} accepted-array of items accepted by DropZone
   * @param {Array} rejected-array of items rejected by DropZone
   */
  onDrop(accepted, rejected) {
    console.log(accepted);
    accepted.map((item, id) => {
      item.id = id;
    });
    rejected.map((item, id) => {
      item.id = id;
    });
    this.setState({ accepted, rejected });
  }
  /**
   * Function manage state of ResultsDialog
   * @public
   * @param {string} type type of ResultDialog
   * @param {boolean} value specify if Dialog is shown
   * @param {string} message message in ResultDialog
   * @param {boolean} hide specify if ResultDialog should be hidden
   * @param {boolean} reset specify if stored data should be reset
   */
  handleDialog(
    type,
    value = true,
    message,
    delay = 1000,
    hide = true,
    reset = true
  ) {
    this.setState({ [type]: value });
    message && this.setState({ message });
    hide && setTimeout(() => this.setState({ [type]: !value }), delay);
    reset && this.setState({ accepted: [], rejected: [], errorMessage: "" });
  }
  /**
   * Function append data to formData object.If key is falsy check for file extensions to check if
   * every frame file have corresponding .obj file(backend restriction).
   * @public
   * @param {Array} data Array of items
   * @param {Object} formData
   * @param {string} key
   */
  appendData(data, formData, key) {
    let result = true;
    const images = [];
    const values = [];
    const errorMessage =
      "Every image should have corresponding .obj file with the same name";
    data.forEach(file => {
      if (key) {
        formData.append(key, file);
      } else {
        const { ext, name } = getExtFromName(file.name);
        if (ext !== "obj") {
          images.push(name);
          formData.append("image", file);
        } else {
          values.push(name);
          formData.append("value", file);
        }
      }
    });
    result = isEqualArray(images, values);
    !result &&
      this.handleDialog("error", true, errorMessage, 4000, undefined, false);
    return result;
  }
  /**
   * Function upload formData on the server.Function use schema to recognize which data type is uploaded
   * @public
   */
  handleUpload() {
    const { name, add } = this.props;
    const { accepted } = this.state;
    const formData = new FormData();
    const schema = schemas[name];
    const result = this.appendData(accepted, formData, schema.key);
    result &&
      API.add(name, formData)
        .then(response => {
          if (response.status === schema.successStatus) {
            schema.store(response.data, name, add);
            this.handleDialog("success");
          }
        })
        .catch(error => {
          this.handleDialog("error");
        });
  }
  /**
   * Function remove item from list of accepted/rejected files
   * @public
   * @param {number} id items id
   * @param {string} type list type
   */
  removeItem(id, type) {
    const stateProperty = type.toLowerCase();
    this.setState({
      [stateProperty]: this.state[stateProperty].filter(item => {
        if (item.id !== id) return item;
      })
    });
  }

  render() {
    const { classes, accept } = this.props;
    const { accepted, rejected, success, error, message } = this.state;
    const notEmpty = accepted.length !== 0 || rejected.length !== 0;
    return (
      <div className={classes.container}>
        <Dropzone
          accept={accept}
          className={classes.dropzone}
          onDrop={this.onDrop}
        >
          <DropzoneIcon />
        </Dropzone>
        {notEmpty && (
          <ListContainer
            accepted={accepted}
            rejected={rejected}
            onUpload={this.handleUpload}
            onClick={this.removeItem}
          />
        )}
        <ResultDialog type="success" open={success} />
        <ResultDialog type="error" open={error} message={message} />
      </div>
    );
  }
}
Dropzone.propTypes = {
  /**
   * Specify which types of files can be added
   */
  accept: propTypes.string,
  /**
   * Add items to Redux store
   */
  add: propTypes.func,
  /**
   * Specify which type of data (textures,frames,script) is added to Redux
   */
  name: propTypes.string
};
export default withStyles(styles)(DropZone);
