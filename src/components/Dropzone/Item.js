/**
 * Created by lukasz on 09.03.18.
 */
import React from "react";
import classNames from "classnames";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import { formatBytes } from "./../../helpers";
import { Clear } from "material-ui-icons";
import {
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText
} from "material-ui/List";
import Avatar from "material-ui/Avatar";
import Tooltip from "material-ui/Tooltip";
import { grey } from "material-ui/colors";
import IconButton from "material-ui/IconButton";
const styles = theme => ({
  item: {
    listStyleType: "none",
    height: 40,
    marginTop: "2px",
    width: "100%"
  },
  secondItem: {
    backgroundColor: grey[200],
    borderRadius: 5
  },
  avatar: {
    height: 30,
    width: 30,
    backgroundColor: grey[300],
    border: "1px solid teal"
  },
  icon: {
    color: "red",
    fontSize: 15
  },
  size: {
    width: "15%",
    display: "flex",
    justifyContent: "flex-end",
    textOverflow: "hidden",
    overflow: "hidden",
    [theme.breakpoints.down("md")]: {
      width: "35%"
    }
  },
  text: {
    width: "85%",
    overflow: "hidden",
    [theme.breakpoints.down("md")]: {
      width: "65%"
    }
  }
});
/**
 * List Item in Dropzone.Uses classnames:
 * @see https://github.com/JedWatson/classnames
 * @requires classnames
 */

const Item = ({ classes, item, onClick, id, type }) => {
  /**
   * @constant {Object} className-provides conditional CSS class rendering
   */
  const className = classNames(classes.item, {
    [classes.secondItem]: id % 2 === 0
  });
  return (
    <ListItem key={id} className={className}>
      {item.type.includes("image/") ? (
        <ListItemAvatar>
          <Avatar className={classes.avatar} src={item.preview} />
        </ListItemAvatar>
      ) : null}
      <ListItemText primary={item.name} className={classes.text} />
      <ListItemText primary={formatBytes(item.size)} className={classes.size} />
      <ListItemSecondaryAction>
        <Tooltip title="Remove" placement="left-end">
          <IconButton
            aria-label="Delete"
            onClick={() => onClick(item.id, type)}
          >
            <Clear className={classes.icon} />
          </IconButton>
        </Tooltip>
      </ListItemSecondaryAction>
    </ListItem>
  );
};
Item.propTypes = {
  /**
   *Item in added/removed list
   */
  item: propTypes.shape({
    id: propTypes.number,
    name: propTypes.string,
    preview: propTypes.string,
    size: propTypes.number,
    type: propTypes.string
  }),
  /**
   *Function remove item from list
   */
  onClick: propTypes.func,
  /**
   * Unique key for every item
   */
  id: propTypes.number
};
export default withStyles(styles)(Item);
