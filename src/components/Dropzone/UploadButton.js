/**
 * Created by lukasz on 08.03.18.
 */
import React from "react";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Button from "material-ui/Button";
import { FileUpload } from "material-ui-icons";
import { indigo } from "material-ui/colors";
const styles = theme => ({
  button: {
    border: `1px solid ${indigo[500]}`,
    borderRadious: 10,
    width: "99%",
    marginLeft: "0.5%",
    marginRight: "0.5%"
  }
});
const UploadButton = ({ classes, onClick }) => {
  return (
    <Button
      variant="flat"
      color="primary"
      fullWidth={true}
      onClick={onClick}
      className={classes.button}
    >
      UPLOAD <FileUpload />
    </Button>
  );
};
UploadButton.propTypes = {
  /**
   * Function which uploads data on server
   */
  onClick: propTypes.func
};
export default withStyles(styles)(UploadButton);
