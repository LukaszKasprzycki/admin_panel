/**
 * Created by lukasz on 01.03.18.
 */
import React, { Fragment } from "react";
import propTypes from "prop-types";
import UploadButton from "./UploadButton";
import { withStyles } from "material-ui/styles";
import List from "./List";
const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    flexDirection: "row",
    maxHeight: 300,
    overflow: "auto",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column"
    }
  }
});
/**
 *Container for List
 */
const ListContainer = ({ accepted, rejected, classes, onClick, onUpload }) => {
  return (
    <Fragment>
      <div className={classes.root}>
        <List type="Accepted" data={accepted} onClick={onClick} />
        <List type="Rejected" data={rejected} onClick={onClick} />
      </div>
      {accepted.length !== 0 && <UploadButton onClick={onUpload} />}
    </Fragment>
  );
};
ListContainer.propTypes = {
  /**
   *Array of accepted item
   */
  accepted: propTypes.array,
  /**
   * Array of rejected item
   */
  rejected: propTypes.array,
  /**
   * Function which removes item from list
   */
  onClick: propTypes.func,
  /**
   * Function which uploads data on server
   */
  onUpload: propTypes.func
};
export default withStyles(styles)(ListContainer);
