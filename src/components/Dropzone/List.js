import React from "react";
import propTypes from "prop-types";
import { green, red } from "material-ui/colors";
import classNames from "classnames";
import { withStyles } from "material-ui/styles";
import Typography from "material-ui/Typography";
import Item from "./Item";
const styles = theme => ({
  list: {
    width: "50%",
    listStyleType: "none",
    marginTop: 10,
    marginBottom: 5,
    marginRight: 5,
    [theme.breakpoints.down("md")]: {
      width: "100%"
    }
  },
  text: {
    fontSize: "bigger",
    paddingLeft: 15,
    fontWeight: "bold",
    paddingBottom: 8
  },
  rejected: {
    color: red[600]
  },
  accepted: {
    color: green[700]
  }
});
/**
 * List of accepted/rejected items in dropzone.Uses:
 * @see https://github.com/JedWatson/classnames
 * @requires classnames
 */
const List = ({ classes, data, type, onClick }) => {
  /**
   * @constant {Object} className provides conditional CSS class rendering
   */
  const className = classNames(classes.text, {
    [classes.rejected]: type === "Rejected",
    [classes.accepted]: type === "Accepted"
  });
  return (
    <div className={classes.list}>
      <Typography className={className}>{type}</Typography>
      {data.map((item, id) => {
        return (
          <Item key={id} item={item} id={id} type={type} onClick={onClick} />
        );
      })}
    </div>
  );
};
List.propTypes = {
  /**
   * Array of items
   */
  data: propTypes.array,
  /**
   *Type of list
   */
  type: propTypes.oneOf(["Accepted", "Rejected"]),
  /**
   * Function which remove item from list
   */
  onClick: propTypes.func
};

export default withStyles(styles)(List);
