import React from "react";
import compose from "recompose/compose";
import grey from "material-ui/colors/grey";
import GetApp from "material-ui-icons/GetApp";
import { withStyles } from "material-ui/styles";
import withWidth from "material-ui/utils/withWidth";

const styles = theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    color: grey[500]
  },
  icon: {
    height: 60,
    width: 60,
    margin: "auto"
  },
  drop: {
    display: "flex",
    flexDirection: "column"
  }
});
/**
 * Icon in Dropzone.Content depends on screen size. See:
 * @see https://github.com/acdlite/recompose
 * @requires recompose
 */
const DropzoneIcon = ({ classes, width }) => {
  /**
   * @constant bigScreen contains bigScrenn width
   * @see https://material-ui-next.com/layout/hidden/
   */
  const bigScreen = ["lg", "xl"];
  return (
    <div className={classes.container}>
      {bigScreen.includes(width) ? (
        <span className={classes.drop}>
          DROP HERE
          <GetApp className={classes.icon} />
        </span>
      ) : (
        <span>TAP HERE TO ADD ITEM</span>
      )}
    </div>
  );
};

export default compose(withStyles(styles), withWidth())(DropzoneIcon);
