/**
 * Created by lukasz on 23.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import Button from "material-ui/Button";
import Dialog, {
  DialogActions,
  DialogContentText,
  DialogContent,
  DialogTitle
} from "material-ui/Dialog";
/**
 * Confirm dialog.Allows to confirm data removal
 */
const ConfirmDialog = ({ open, close, deleteItem }) => {
  return (
    <Dialog open={open} onClose={close}>
      <DialogTitle>{"Are You Sure?"}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          You will not be able to recover this file!
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={close} color="primary">
          Cancel
        </Button>
        <Button onClick={deleteItem} color="secondary" autoFocus>
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
};
ConfirmDialog.defaultProps = {
  open: false
};
ConfirmDialog.propTypes = {
  /**
   * ConfirmDialog status.If true is showed,if false is hidden.
   */
  open: propTypes.bool,
  /**
   * Function invoken on close
   */
  close: propTypes.func,
  /**
   * Function invoken on delete request
   */
  deleteItem: propTypes.func
};

export default ConfirmDialog;
