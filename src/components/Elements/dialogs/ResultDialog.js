/**
 * Created by lukasz on 27.02.18.
 */
import React, { Fragment } from "react";
import propTypes from "prop-types";
import classNames from "classnames";
import Snackbar from "material-ui/Snackbar";
import { CheckCircle, ErrorOutline } from "material-ui-icons";
import { withStyles } from "material-ui/styles";
import Slide from "material-ui/transitions/Slide";
import { green, grey, red } from "material-ui/colors";
const styles = theme => ({
  success: {
    backgroundColor: green[300]
  },
  span: {
    position: "relative",
    bottom: 4,
    marginRight: 10
  },
  error: {
    backgroundColor: red[300]
  },
  snackbar: {
    color: grey[100],
    justifyContent: "center",
    fontSize: 16,
    borderRadius: 10,
    marginBottom: 60
  },
  standard: {
    maxWidth: 200
  }
});

function Transition(props) {
  return <Slide direction="up" {...props} />;
}
/**
 * ResultDialog is showed after success and error
 * @see https://github.com/JedWatson/classnames
 * @requires classnames
 */

const ResultDialog = ({ classes, type, message, open }) => {
  const isSuccess = type === "success";
  /**
   * @constant {Object} className-provides conditional CSS class rendering
   */
  const className = classNames(classes.snackbar, {
    [classes.success]: isSuccess,
    [classes.error]: !isSuccess,
    [classes.standard]: !message
  });
  const style = {
    classes: {
      root: className
    },
    icon: isSuccess ? <CheckCircle /> : <ErrorOutline />,
    text: isSuccess ? "Success" : "Error occurred"
  };
  return (
    <Snackbar
      open={open}
      transition={Transition}
      anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
      message={
        <Fragment>
          <span className={classes.span}>
            {!message ? style.text : message}
          </span>
          {style.icon}
        </Fragment>
      }
      SnackbarContentProps={style}
    />
  );
};

ResultDialog.propTypes = {
  /**
   * Message shown in ResultDialog.By default Success(if success dialog) or Error occured(if error dialog)
   */
  message: propTypes.string,
  /**
   * Specify type of ResultDialog(success or error)
   */
  type: propTypes.string,
  /**
   * ResultDialog status.If true is showed,if false is hidden.
   */
  open: propTypes.bool
};
ResultDialog.defaultProps = {
  type: "success"
};

export default withStyles(styles)(ResultDialog);
