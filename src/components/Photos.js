/**
 * Created by lukasz on 22.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import Panel from "./Panel/Panel";
import { add } from "../actions";
import { connect } from "react-redux";
const Photos = ({ data, history, logOut, add }) => {
  const tableHead = [
    {
      id: "image",
      label: "Image",
      sortable: false
    },
    {
      id: "status",
      label: "",
      sortable: false
    },
    {
      id: "name",
      label: "Name",
      sortable: true
    },
    {
      id: "createdAt",
      label: "Created",
      sortable: true
    },
    {
      id: "process",
      label: "Process",
      sortable: false
    },
    {
      id: "activate",
      label: "Activate",
      sortable: false
    },
    {
      id: "json",
      label: "JSON",
      sortable: false
    },
    {
      id: "delete",
      label: "Delete",
      sortable: false
    }
  ];
  return (
    <Panel
      tableHead={tableHead}
      data={data}
      dropzone={false}
      logOut={logOut}
      name="photos"
      add={add}
    />
  );
};
Photos.propTypes = {
  /**
   *Array of objects in shape:
   */
  data: propTypes.arrayOf(
    propTypes.shape({
      id: propTypes.number,
      name: propTypes.string,
      createdAt: propTypes.string,
      image: propTypes.string,
      value: propTypes.string
    })
  ),
  /**
   * History Object from React Router
   */
  history: propTypes.object,
  /**
   * Function log user out of application
   */
  logOut: propTypes.func
};
export default connect(null, { add })(Photos);
