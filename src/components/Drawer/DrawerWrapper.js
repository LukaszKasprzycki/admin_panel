/**
 * Created by lukasz on 26.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Drawer from "material-ui/Drawer";
import Hidden from "material-ui/Hidden";
import DrawerContent from "./DrawerContent";
const styles = theme => ({
  drawerPaper: {
    backgroundColor: "black",
    width: 250,
    [theme.breakpoints.up("md")]: {
      position: "relative"
    }
  },
  drawerWrapper: {
    backgroundColor: "black"
  }
});
/**
 * DrawerContent Container.Returned Component depends on screen size
 */
const DrawerWrapper = ({
  classes,
  open,
  handleDrawerToggle,
  theme,
  history,
  user
}) => {
  return (
    <div className={classes.drawerWrapper}>
      <Hidden mdUp>
        <Drawer
          variant="temporary"
          anchor={theme.direction === "rtl" ? "right" : "left"}
          open={open}
          classes={{
            paper: classes.drawerPaper
          }}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          <DrawerContent user={user} history={history} />
        </Drawer>
      </Hidden>
      <Hidden smDown implementation="css">
        <Drawer
          variant="permanent"
          open
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <DrawerContent user={user} history={history} />
        </Drawer>
      </Hidden>
    </div>
  );
};
DrawerWrapper.propTypes = {
  /**
   * History object from React Router
   */
  history: propTypes.object,
  /**
   *User data.By default  undefined.
   When user is logged object in shape:
   */
  user: propTypes.shape({
    username: propTypes.string,
    id: propTypes.number
  }),
  /**
   * Function toggle responsive drawer
   */
  handleDrawerToggle: propTypes.func
};

export default withStyles(styles)(DrawerWrapper);
