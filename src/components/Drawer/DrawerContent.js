/**
 * Created by lukasz on 26.02.18.
 */
import React, { Fragment } from "react";
import DrawerHeader from "./DrawerHeader";
import { Photo, Code, Wallpaper, Menu } from "material-ui-icons";
import LinkButton from "./LinkButton";
import propTypes from "prop-types";
/**
 *Contains LinkButtons to specific routes
 */
const DrawerContent = ({ user, history }) => {
  const { pathname } = history.location;
  return (
    <Fragment>
      <DrawerHeader user={user} />
      <LinkButton selected={pathname === "/script"} to="/script" label="Script">
        <Code />
      </LinkButton>
      <LinkButton
        selected={pathname === "/textures"}
        to="/textures"
        label="Textures"
      >
        <Menu />
      </LinkButton>
      <LinkButton selected={pathname === "/photos"} to="/photos" label="Photos">
        <Photo />
      </LinkButton>
      <LinkButton selected={pathname === "/frames"} to="/frames" label="Frames">
        <Wallpaper />
      </LinkButton>
    </Fragment>
  );
};
DrawerContent.propTypes = {
  /**
   *User data.By default  undefined.
   When user is logged object in shape:
   */
  user: propTypes.shape({
    username: propTypes.string,
    id: propTypes.number
  }),
  /**
   * History object from React Router
   */
  history: propTypes.object
};
export default DrawerContent;
