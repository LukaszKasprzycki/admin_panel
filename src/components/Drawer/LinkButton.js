/**
 * Created by lukasz on 26.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import classNames from "classnames";
import { Link } from "react-router-dom";
import { ListItem, ListItemIcon, ListItemText } from "material-ui/List";
import { withStyles } from "material-ui/styles";
import { teal } from "material-ui/colors";

const styles = theme => ({
  itemIcon: {
    color: "white"
  },
  link: {
    textDecoration: "none",
    marginBottom: 20
  },
  item: {
    marginBottom: 20,
    borderRadius: 5,
    width: "80%",
    marginLeft: "10%",
    backgroundColor: "inherit",
    transition: theme.transitions.create("white", {
      duration: theme.transitions.duration.shortest
    }),
    "&:hover": {
      textDecoration: "none",
      backgroundColor: teal[600],
      "&disabled": {
        backgroundColor: "transparent"
      }
    },
    "&:focus": {
      backgroundColor: teal[600]
    }
  },
  itemText: {
    fontWeight: 500,
    color: "white"
  },
  selected: {
    backgroundColor: teal[600]
  }
});
/**
 * LinkButton redirect to specyfic Route.Uses classnames
 * @requires classNames
 * @see https://github.com/JedWatson/classnames
 */

const LinkButton = ({ classes, to, label, children, selected }) => {
  /**
   *Provides conditional CSS class rendering
   */
  const className = classNames(classes.item, {
    [classes.selected]: selected
  });
  return (
    <Link to={to} className={classes.link}>
      <ListItem
        button
        classes={{
          button: className
        }}
      >
        <ListItemIcon className={classes.itemIcon}>{children}</ListItemIcon>
        <ListItemText
          classes={{
            primary: classes.itemText
          }}
          primary={label}
        />
      </ListItem>
    </Link>
  );
};

LinkButton.propTypes = {
  /**
   * String to specific Route
   */
  to: propTypes.string,
  /**
   * String with content of the Button
   */
  label: propTypes.string,
  /**
   *Icon in Button
   */
  children: propTypes.element,
  /**
   * LinkButton status.If true background color is teal[600]
   */
  selected: propTypes.bool
};

export default withStyles(styles)(LinkButton);
