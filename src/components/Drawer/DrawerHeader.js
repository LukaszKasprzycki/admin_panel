/**
 * Created by lukasz on 26.02.18.
 */
import React from "react";
import propTypes from "prop-types";
import { grey, teal } from "material-ui/colors";
import Avatar from "material-ui/Avatar";
import { withStyles } from "material-ui/styles";
import { ListItemText } from "material-ui/List";
const styles = theme => ({
  container: {
    height: "auto",
    minHeight: "10vh",
    backgroundColor: "black",
    display: "flex",
    verticalAlign: "middle"
  },
  avatar: {
    backgroundColor: teal[600],
    marginRight: 5,
    marginLeft: 5
  },
  display: {
    display: "flex",
    flexDirection: "row"
  },
  text: {
    display: "flex",
    flexDirection: "column",
    border: "1px solid blue",
    verticalAlign: "middle",
    height: "100%",
    lineHeight: "100%",
    position: "relative"
  },
  test: {
    width: "90%",
    marginLeft: "5%",
    marginRight: "5%",
    marginTop: "15%",
    marginBottom: "15%",
    borderRadius: 5,
    backgroundColor: "inherit",
    display: "flex",
    position: "relative",
    alignItems: "center",
    justifyContent: "space-around",
    overflow: "hidden",
    textOverflow: "..."
  },
  block: {
    background: "white",
    opacity: 0.3,
    position: "absolute",
    top: 0,
    left: 0,
    height: "100%",
    width: "100%",
    borderRadius: 5
  },
  primary: {
    color: "white",
    fontWeight: "bold",
    fontSize: 14,
    overflowX: "hidden",
    margin: 0
  },
  secondary: {
    color: grey[400],
    fontSize: 13
  },
  root: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 0
  }
});
/**
 * @public
 * Display user info:username and useremail(waiting for backend implementation)
 */
const DrawerHeader = ({ classes, user }) => {
  return (
    <div className={classes.container}>
      <div className={classes.test}>
        <div className={classes.block} />
        <Avatar className={classes.avatar}>
          {user && user.username.charAt(0).toUpperCase()}
        </Avatar>
        <ListItemText
          classes={{
            root: classes.root,
            primary: classes.primary,
            secondary: classes.secondary
          }}
          primary={user && user.username}
          secondary={`${user && user.username}@gmail.com`}
        />
        {/*Expect to have useremail property in future*/}
      </div>
    </div>
  );
};
DrawerHeader.propTypes = {
  /**
   *User data.By default  undefined.
   When user is logged object in shape:
   */
  user: propTypes.shape({
    username: propTypes.string,
    id: propTypes.number
  })
};
DrawerHeader.defaultProps = {
  user: {
    username: "Unknown User"
  }
};

export default withStyles(styles)(DrawerHeader);
