/**
 * Created by lukasz on 26.02.18.
 */
import React from "react";
import { withStyles } from "material-ui/styles";
import { Switch, Route, Redirect } from "react-router-dom";
import Script from "./Script";
import Textures from "./Textures";
import Frames from "./Frames";
import Photos from "./Photos";
import { grey } from "material-ui/colors";
import PropTypes from "prop-types";

const styles = theme => ({
  content: {
    backgroundColor: grey[50],
    width: "100%",
    marginTop: 56,
    [theme.breakpoints.up("md")]: {
      marginTop: 0
    }
  }
});
/**
 *AdminPanel content.Contains routes to '/frames','/script','photos','textures'
 */
const Content = ({
  classes,
  textures,
  frames,
  photos,
  script,
  history,
  logOut
}) => {
  return (
    <main className={classes.content}>
      <Switch>
        <Route
          exact
          path="/script"
          render={() => (
            <Script history={history} script={script} logOut={logOut} />
          )}
        />
        <Route
          exact
          path="/textures"
          render={() => (
            <Textures history={history} data={textures} logOut={logOut} />
          )}
        />
        <Route
          exact
          path="/frames"
          render={() => (
            <Frames history={history} data={frames} logOut={logOut} />
          )}
        />
        <Route
          exact
          path="/photos"
          render={() => (
            <Photos history={history} data={photos} logOut={logOut} />
          )}
        />
        <Redirect to="/textures" />
      </Switch>
    </main>
  );
};
Content.propTypes = {
  /**
   * Photos data stored in Redux.
   * By default [].
   * After data is fetched Array of objects with shape:
   */
  photos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      createdAt: PropTypes.string,
      image: PropTypes.string,
      value: PropTypes.string
    })
  ),
  /**
   * Frames data stored in Redux.
   * By default [].
   * After data is fetched Array of objects with shape:
   */
  frames: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      createdAt: PropTypes.string,
      image: PropTypes.string,
      value: PropTypes.string
    })
  ),
  /**
   * Textures data stored in Redux.
   * By default [].
   * After data is fetched Array of objects with shape:
   */
  textures: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      createdAt: PropTypes.string,
      image: PropTypes.string,
      value: PropTypes.string
    })
  ),
  /**
   * Script object stored in Redux.
   * By default null.
   * After upload Object
   */
  script: PropTypes.object
};
export default withStyles(styles)(Content);
