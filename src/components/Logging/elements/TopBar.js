import React from "react";
import Typography from "material-ui/Typography";
import Toolbar from "material-ui/Toolbar";
import AppBar from "material-ui/AppBar";
import teal from "material-ui/colors/teal";
import { withStyles } from "material-ui/styles";
const styles = theme => ({
  appbar: {
    backgroundColor: "#000",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    color: teal[300],
    fontWeight: 600
  }
});
/**
 * Topbar in LoginPanel
 */
const TopBar = ({ classes }) => {
  return (
    <AppBar className={classes.appbar} elevation={2}>
      <Toolbar>
        <Typography variant="title" color="inherit">
          augmenter admin
        </Typography>
      </Toolbar>
    </AppBar>
  );
};
export default withStyles(styles)(TopBar);
