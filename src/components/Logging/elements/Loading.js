import React from "react";
import { withStyles } from "material-ui/styles";
import { CircularProgress } from "material-ui/Progress";
const styles = theme => ({
  container: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  }
});
/**
 * Loading component shown when loading=true
 */
const Loading = ({ classes }) => {
  return (
    <div className={classes.container}>
      <CircularProgress size={50} thickness={7} />
      <br />
      Loading...
    </div>
  );
};
export default withStyles(styles)(Loading);
